import UIKit
import CoreMotion


class ViewController: UIViewController {
   
   
   
   
   let motionManager = CMMotionManager()
   var textField = UITextField()
   var animator = UIDynamicAnimator()
//   var motionLastYaw = 0.0
   var letters = [UILabel]()
   var gravity = UIGravityBehavior()
   var collision = UICollisionBehavior()
   
   
   override func viewDidLoad() {
      super.viewDidLoad()

      view.backgroundColor = UIColor.lightGray
      
      textField = UITextField(frame: CGRect(x:0, y:0, width:screenWidth, height:100))
      textField.placeholder = "Введите текст"
      textField.font = UIFont.systemFont(ofSize: 20)
      textField.borderStyle = UITextBorderStyle.roundedRect
      textField.autocorrectionType = UITextAutocorrectionType.no
      textField.keyboardType = UIKeyboardType.default
      textField.returnKeyType = UIReturnKeyType.done
      textField.clearButtonMode = UITextFieldViewMode.whileEditing;
      textField.contentVerticalAlignment = UIControlContentVerticalAlignment.center
      textField.delegate = self
      self.view.addSubview(textField)
      
      if !motionManager.isGyroAvailable {
         print("NO GYRO")
         return
      }
      
      
      
      motionManager.gyroUpdateInterval = 1.0 / 60.0
      
      
      motionManager.startDeviceMotionUpdates(to: OperationQueue.current!, withHandler:{
         deviceManager, error in
//         let xAcceleration = deviceManager?.rotationRate.x
//         let yAcceleration = deviceManager?.rotationRate.y
         
         
         
        
         self.motionManager.startAccelerometerUpdates()

         guard let accX = self.motionManager.accelerometerData?.acceleration.x          else { return}

         guard let accY = self.motionManager.accelerometerData?.acceleration.y
            else { return}
         self.gravity.gravityDirection = CGVector(dx: accX * 20, dy: accY * -20)
         self.collision.translatesReferenceBoundsIntoBoundary = true
        
         for l in self.letters {
            self.gravity.addItem(l)
            self.collision.addItem(l)
         }
         
         print(accY)
         

      })
      
      
      
        animator = UIDynamicAnimator(referenceView: self.view)
      animator.addBehavior(gravity)
      animator.addBehavior(collision)
      
      
   }
   
   func createLetters(string: String) {
      print(string)
      var x = 0
      let characters = Array(string.characters)
      for c in characters {
         let l = UILabel(frame: CGRect(x: x, y:Int(screenHeight - 20), width:20, height:20))
         l.text = String(c)
         l.backgroundColor = UIColor.white
         l.layer.cornerRadius = 3
         
         x += 20
         self.letters.append(l)
         self.view.addSubview(l)
      }
      
      for l in self.letters {
         gravity.addItem(l)
         collision.addItem(l)
      }
      
      
   }
   
   

   

}


extension ViewController : UITextFieldDelegate {
   
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      createLetters(string:self.textField.text!)
      self.textField.text = ""
      self.textField.resignFirstResponder()
      return true
   }
   

}

